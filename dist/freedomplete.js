(function () {

    /* constructeur */
    var freedomplete = function (params) {
        /* les paramètre par défaut */
        var defaultParams = {
            inputID : '',
            htmlCustomClass : '',

            /*
            si les valeurs a check existent déja
            d'autre valeurs de l'objet dépendent de celle-ci
            */
            areValsExisting : false,
            absolutePos : false,
            sameWidth : true,
            hasStyle : false,
            valContainer : '',
            valsParentID : '',
            valToTarget : '',

            /* si on doit gérer le showing on doit aussi gérer le positionnement donc on doit check si il y a un parent en positon relative */
            handleShowing : true,
            parentRelative: false,
            parentRelativeID : '',

            /* si les valeurs n'existent pas, elles seront envoyée avec values */
            values : [],

            /* on check le mode de vérification voulu */
            mode : 'keypress',  /* 2 valeurs possibles => keypress, sendingInput */
            timeoutKeypress: 300,
            buttonID : '',

            /* si il faut cliquer pour appliquer la valeur */
            clickToApply : true,
            multipleVal : false,
            ajaxMode : false,
            ajaxParams : {
                url : '',
                method : 'POST',
                nounceMode : 'headers',
                nounceName : '',
                token : '',
                valueName : 'textSended',
                additionalVal : {},
                customizeInputName : {}
            },
            applyCallback : false
        };

        /* déclaration des variables */
        var valContainer,
            paramsSize,
            contentInDiv,
            valsParent,
            isTimeoutSet = false,
            input,
            timeoutSet = false,
            freedompleteTimeoutSet,
            parentRelative,
            self = this;

        this.isInputClicked = false;

        /* taille de l'objet pour voir si il contient quelque chose */
        paramsSize = Object.keys(params).length;

        /* on crée la variable de parametres finale */
        if( typeof(params) === 'object' && paramsSize > 0 ){
            for( var key in params ){
                if( typeof(defaultParams[key]) !== 'undefined' && typeof(defaultParams[key]) !== 'object'){
                    defaultParams[key] = params[key];
                }
                else if( typeof(defaultParams[key]) !== 'undefined' && typeof(defaultParams[key]) == 'object' ){
                    for( var key2 in params[key] ){
                        defaultParams[key][key2] = params[key][key2];
                    }
                }
            }
        }

        this.params = defaultParams;

        var existingHiddenFields = document.querySelectorAll('input.freedompleteOf'+this.params.inputID+'[type="hidden"]'),
            existingHiddenFieldsL = existingHiddenFields.length;
        this.hiddenFields = {};
        for( var i = 0; i < existingHiddenFieldsL; i++ ){
          this.hiddenFields[i] = existingHiddenFields[i];
        }

        /* fonction de creation de div */
        this.createList = function (vals){
            var frag = document.createDocumentFragment();

            /* création de la div parente */
            var list = document.createElement('ul');
            list.style.display = 'none';
            list.className = this.input.id+'ValueParent';
            if( this.params.htmlCustomClass.length > 0 ){
              list.className += this.params.htmlCustomClass;
            }

            /* création du contenu */
            if( vals != undefined ){
                var i = 0,
                    valsL = vals.length;

                for(i = 0; i < valsL; i++){
                    var li = document.createElement('li');
                    li.className = this.params.valContainer.substr(1);
                    li.textContent = vals[i];
                    if( this.params.clickToApply === true ){
                      li.addEventListener('mousedown', freedompleteClickToApplyVal.bind(this), false);
                    }
                    list.appendChild(li);
                }
            }

            /* on applique toutes les données */
            frag.appendChild(list);

            if( this.params.parentRelative == true ){
                parentRelative = document.getElementById( this.params.parentRelativeID );

                if( typeof parentRelative == 'undefined' ){
                    console.error('freedomplete : parentRelativeID does not exist');
                }

                if( this.input.nextElementSibling != null ){
                  parentRelative.insertBefore(frag, this.input.nextElementSibling);
                }
                else{
                  parentRelative.appendChild( frag );
                }
            }
            else{
                document.body.appendChild(frag);
            }

            valsParent = list;
            this.valsParent = list;
        };

        /* si le bloc des valeurs existe déja, on le retrouve */
        this.getValContainer = function(){
            if( typeof(this.params.valContainer) === 'string' && this.params.valContainer.length > 0 ){
              valContainer = document.querySelectorAll( this.params.valContainer );

              /* check de si on doit gérer le click ou pas */
              if( this.params.clickToApply === true && this.params.ajaxMode == false ){
                var valContainersL = valContainer.length;
                for( var i = 0; i < valContainersL; i++ ){
                  valContainer[i].addEventListener('mousedown', freedompleteClickToApplyVal.bind(this), false);
                }
              }
              this.valContainers = valContainer;
            }
            else{
               console.error('freedomplete : Bad or no valContainer specified');
            }
        };

        this.positionValsCont = function () {
            var inputPos = input.getBoundingClientRect(),
                inputHeight = input.clientHeight,
                inputWidth = input.clientWidth;

            valsParent.style.position = 'absolute';
            valsParent.style.zIndex = '10000';

            if( this.params.parentRelative === true ){
                if( typeof(this.params.parentRelativeID) === 'string' && this.params.parentRelativeID.length > 0 ){

                    var relativeParent = document.getElementById( this.params.parentRelativeID );

                    if( relativeParent == undefined ){
                        alert('Bad parentRelativeID !');
                    }
                    else{
                        var parentPos = relativeParent.getBoundingClientRect();

                        var top = (inputPos.top - parentPos.top)+inputHeight;
                        var left = inputPos.left - parentPos.left;

                        valsParent.style.top = top+'px';
                        valsParent.style.left = left+'px';
                    }
                }
                else{
                    alert('no parentRelativeID !');
                }
            }
            else{
                var top = inputPos.top + inputHeight;
                var left = inputPos.left;

                valsParent.style.top = top+'px';
                valsParent.style.left = left+'px';
                valsParent.style.minWidth = inputWidth+'px';
            }

            /* fonction pour check si un parent est en position relative pour le calcul de position */
        };

        /* fonction de creation de style */
        this.applyStyle = function (){

        };

        /* fonction de création de style minimum */
        this.applyMinimumStyle = function (){
            var head = document.head || document.getElementsByTagName('head')[0];

            var miniStyle = document.createElement('style');
            miniStyle.type = 'text/css';

            var cssRule = document.createTextNode(this.params.valContainer+'{min-width:'+input.clientWidth+'px;}');
            miniStyle.appendChild( cssRule );

            head.appendChild( miniStyle );
        };

        /* initialisation */
        this.init = function (){

            /* on check si inputID est rempli */
            if( typeof(this.params.inputID) === 'string' && this.params.inputID.length > 0 ){
                input = document.getElementById( this.params.inputID );
                this.input = input;
            }
            else{
                console.error('freedomplete : Bad or no inputID is specified !');
            }

            /* on check le valsparent si il existe déja */
            if( this.params.valsParentID.length > 0  ){
                valsParent = document.getElementById( this.params.valsParentID );
                this.valsParent = valsParent;
                if( typeof valsParent == 'undefined' ){
                    console.error('freedomplete : valParentID does\'nt exist');
                }
                else{
                  if( this.params.handleShowing == true ){
                    this.valsParent.style.display = 'none';
                  }
                }
            }

            /* si les valeurs existent, on les récupèrent */
            if( this.params.areValsExisting === true ){
                this.getValContainer();
            }
            else{
              this.params.valContainer = '.'+this.params.inputID+'Value';
              this.params.valToTarget = this.params.valContainer;

              if( this.params.ajaxMode == false ){
                  this.createList( this.params.values );
              }
              else{
                  this.createList();
              }
            }

            /* check du valcontainer valtotarget */
            if( typeof (this.params.valToTarget) === 'string' && this.params.valToTarget.length === 0 ){
              this.params.valToTarget = this.params.valContainer;
            }

            /* si le position du bloc existant est absolu, alors on récupère la pos de l'input et on applique le bloc juste en dessous */
            if( this.params.absolutePos === true ){
                this.positionValsCont();
            }

            /* on crée le style ou le style minimum nécéssaire */
            if( this.params.hasStyle === true ){
                if( this.params.absolutePos === true ){
                    this.applyMinimumStyle();
                }
            }
            else{
                this.applyStyle();
            }

            /* on test le parent relatif */
            if( this.params.parentRelative == true ){
                parentRelative = document.getElementById( this.params.parentRelativeID );
                if( typeof parentRelative == 'undefined' ){
                    console.error('freedomplete : parentrelativeID does\'nt exist');
                }
            }

            /* on gère la taille */
            if( this.params.sameWidth == true ){
                var width = input.offsetWidth;
                valsParent.style.width = width;
            }

            /* on gère l'affichage */
            if( this.params.handleShowing === true ){
              this.freedompleteToggleShowingBinded = freedompleteToggleShowing.bind(this);
              this.input.addEventListener('focus', this.freedompleteToggleShowingBinded, false);
              this.input.addEventListener('blur', this.freedompleteToggleShowingBinded, false);
            }

            /* on test le mode demandé */
            if( this.params.mode === 'keypress' ){
                input.addEventListener('keyup', freedompleteListenKeypress.bind(this), false);
            }
            else if( this.params.mode === 'sendingInput' ){
                if( typeof(this.params.buttonID) == 'string' && this.params.buttonID.length > 0 ){
                    document.getElementById( this.params.buttonID ).addEventListener('click', freedompleteListenClick.bind(this), false);
                    document.getElementById( this.params.inputID ).addEventListener('keypress', freedompletePreventkeypressEnter.bind(this), false);
                }
                else{
                    alert('No button ID specified !');
                }
            }
            else{
                alert('Bad or no mode specified !');
            }


        };

        /*
        ***********
        les fonctions appellées par l'objet
        ***********
        */
        /* ecoute sur le click du bouton d'envoi si mode envoi */
        function freedompleteListenClick(event){
            event.preventDefault();
            var inputValue = input.value;
            var l = valContainer.length;
            var regex = new RegExp(inputValue, 'i');

            if( inputValue.length > 0 ){
                for(var i = 0; i < l; i++){
                    var targetedVal = targetValue( this.valContainers[i], this );

                    /* on lui applique le style nécéssaire */
                    var resultatRegex = regex.test( targetedVal.textContent );

                    if( resultatRegex == true ){
                        this.valContainers[i].style.display = '';
                    }
                    else{
                        this.valContainers[i].style.display = 'none';
                    }
                }
            }
            else{
                for(var i = 0; i < l; i++){
                    var targetedVal = targetValue( this.valContainers[i], this );
                    this.valContainers[i].style.display = '';
                }
            }
        }

        /* ecoute de l'événement keypress */
        function freedompleteListenKeypress(event){
          var curInst = this;
          if( timeoutSet === true ){

              /* clear du timeout */
              timeoutSet = false;
              clearTimeout(freedompleteTimeoutSet);

              /* reset du timeout */
              timeoutSet = true;

              /* check du mode ajax */
              if( curInst.params.ajaxMode == true ){
                  freedompleteTimeoutSet = setTimeout(function(){freedompleteSendAjaxReq(curInst);}, this.params.timeoutKeypress);
              }
              else{
                  freedompleteTimeoutSet = setTimeout(function(){freedompleteParcourValues(curInst);}, this.params.timeoutKeypress);
              }

          }
          else{
              timeoutSet = true;

              /* check du mode ajax */
              if( curInst.params.ajaxMode == true ){
                  freedompleteTimeoutSet = setTimeout(function(){freedompleteSendAjaxReq(curInst);}, this.params.timeoutKeypress);
              }
              else{
                  freedompleteTimeoutSet = setTimeout(function(){freedompleteParcourValues(curInst);}, this.params.timeoutKeypress);
              }
          }

        }
        function freedompleteParcourValues( curInst ){
            /* on passe la variable a false vu que le timeout s'est executé */
            timeoutSet = false;

            /* la fonction pour traiter les valeurs */
            var regex = new RegExp( input.value );

            var l  = valContainer.length;

            for( var i = 0; i < l; i++ ){
                var targetedVal = targetValue( curInst.valContainers[i], curInst );

                if( regex.test( targetedVal.textContent ) ){
                    curInst.valContainers[i].style.display = '';
                }
                else{
                    curInst.valContainers[i].style.display = 'none';
                }
            }
        }

        /* fonction d'affichage du bloc ou non */
        // function freedompleteHandleFocus(event){
        //     valsParent.style.display = 'block';
        // }
        function freedompleteToggleShowing(event){
          if( this.valsParent.style.display == 'none' ){
            this.valsParent.style.display = 'block';
          }
          else{
            this.valsParent.style.display = 'none';
          }
        }

        function targetValue( obj, curInst ){
          if( curInst.params.valToTarget !== curInst.params.valContainer ){
            var obj = obj.querySelector( curInst.params.valToTarget );
          }

          return obj;
        }

        /* fonction pour ecouter le click et faire disparaitre les valeurs si nécéssaire */
        function freedompleteClickToApplyVal(event){
          event.preventDefault();
          event.stopImmediatePropagation();
          var cible = event.target;

          if( cible.classList.contains( this.params.valContainer.slice(1) ) ){
              input.value = cible.textContent.trim();
          }
          else{
            while( !cible.classList.contains( this.params.valContainer.slice(1) ) ){
              cible = cible.parentNode;
            }
            var targetedVal = cible.querySelector( this.params.valToTarget );
            input.value = targetedVal.textContent.trim();
          }

          if( this.params.handleShowing == true ){
            this.input.blur();
          }

          /* on regarde l'element qu'on a cliqué */
          // if( withShowing === true && valsParent.style.display !== 'none' && cible.classList.contains( this.params.valContainer.slice(1) )){
          //     valsParent.style.display = 'none';
          // }
        }

        /* fonction du prevent enter pour eviter d'envoyer le formulaire par mégarde */
        function freedompletePreventkeypressEnter(event){
          if( event.key == 'Enter' ){
            var freedompleteListenClickCall = freedompleteListenClick.bind(this);
            freedompleteListenClickCall(event);
          }
        }

        /* fonction pour envoyer le xhr après le keypress */
        function freedompleteSendAjaxReq(curInst){

            /*test de si valsparent contient quelque chose*/
            while( valsParent.firstElementChild ){
                valsParent.firstElementChild.remove();
            }

            /* fonction d'envoi de l'ajax */
            var xhr = new XMLHttpRequest();
            var fd = new FormData();

            xhr.open( this.params.ajaxParams.method, this.params.ajaxParams.url );
            xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
            if( this.params.ajaxParams.nounceMode == 'headers' ){
                xhr.setRequestHeader( this.params.ajaxParams.nounceName, this.params.ajaxParams.token );
            }
            else if( this.params.ajaxParams.nounceMode == 'value' ){
                fd.append( this.params.ajaxParams.nounceName, this.params.ajaxParams.token);
            }
            fd.append( this.params.ajaxParams.valueName, document.getElementById( this.params.inputID ).value );

            /* add multiple params */
            for( var key in this.params.ajaxParams.additionalVal ){
                fd.append(key, this.params.ajaxParams.additionalVal[key]);
            }

            if( curInst.input.value.length > 0 ){
                xhr.send(fd);
            }

            xhr.onreadystatechange = function(){
                if( xhr.readyState == 4 ){
                    var resp = JSON.parse(xhr.response);

                    for(var respKey in resp){
                        /* si il faut des valeurs sup, on envoit un objet dans un objet, la str a afficher dans le input doit etre en première position */
                        if( typeof resp[respKey] == 'object' ){
                            var ligne = document.createElement('li'),
                                compteur = 0;

                            for(var respKey2 in resp[respKey]){
                                if( compteur == 0 ){
                                    ligne.textContent = resp[respKey][respKey2];
                                }
                                else{
                                    ligne.setAttribute('data-'+respKey2, resp[respKey][respKey2]);
                                }
                                compteur++;
                            }
                        }
                        else{
                        /* sinon la valeur est une est chaine de caractère */
                            var ligne = document.createElement('li');
                            ligne.textContent = resp[respKey];
                        }

                        var bindedClickToApplyAfterAjax = clickToApplyAfterAjax.bind(curInst);
                        ligne.addEventListener('click', bindedClickToApplyAfterAjax, false);
                        curInst.valsParent.appendChild( ligne );
                    }
                }
            }
        }

        /* appliquer la valeur après l'ajout des valeurs en ajax */
        function clickToApplyAfterAjax(event){
            event.preventDefault();

            var elem = event.target;
            input.value = elem.textContent.trim();

            var hiddenFields = this.hiddenFields;

            console.log('hiden fields', hiddenFields);

            if( this.params.multipleVal == false ){
              for( var key in hiddenFields ){
                  hiddenFields[key].remove();
              }
            }

            for(var key in elem.dataset){
                var hiddenField = document.createElement('input');
                hiddenField.setAttribute('type', 'hidden');
                hiddenField.className = 'freedompleteOf'+this.input.id;

                if(  Object.keys(this.params.ajaxParams.customizeInputName).length > 0 ){
                    hiddenField.setAttribute('name', this.params.ajaxParams.customizeInputName[key]);
                }
                else{
                    this.params.ajaxParams.customizeInputName
                    hiddenField.setAttribute('name', key);
                }
                hiddenField.setAttribute('value', elem.dataset[key]);
                input.parentNode.appendChild( hiddenField );

                hiddenFields[key] = hiddenField;
            }

            this.valsParent.style.display = 'none';

            /* on vide la liste des résultats */
            while( valsParent.firstElementChild ){
              valsParent.firstElementChild.remove();
            }
            this.input.setAttribute('value', input.value);
            this.input.focus();

            /* callback */
            if( typeof this.params.applyCallback == 'function' ){
              this.params.applyCallback();
            }

            this.hiddenFields = hiddenFields;

        }

        /* initialisation */
        this.init();

        /* on retourne l'objet créé */
        return this;
    };



    /* On ajoute l'instance dans une variable globale */
    window.freedomplete = freedomplete;

}());

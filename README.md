# Freedomplete

Freedomplete was born to give to the user a maximum of freedom to the user. It's not the first autocomplete that you can find on the web but it's the one that will give you the maximum freedom.

## Adding freedomplete to your project

Freedomplete was made with pure js so you don't need jQuery to use it.

```
<script src="freedomplete.js"></script>
var instance = new freedomplete({optionsObject});
```

## Documentation

Freedomplete has a bunch of parameters that you can use to customize it. You need to pass them in a javascript object.  
The ajax mode use the XMLHttpRequest and the FormData API with the header X-Requested-With. The ajax response need to be a JSON object. You can pass multiple data in one index it will be passed as data attribute.


### Parameters

| Index | Type | Description | Default value |
| --- | --- | --- | --- |
| inputID | String | input ID on wich one you want to instantiate freedomplete | empty |
| htmlCustomClass | String | the class you want to pass to the list of values | empty |
| areValsExisting | Boolean | if the value are already existing or if freedomplete need to create the list itself | false |
| valContainer | String | If the vals are already existing, pass the container class. Should be the same for every value  | empty |
| valToTarget | String | The className ot the val direct container node | empty |
| absolutePos | Boolean | Is the values parent in position absolute | false |
| sameWidth | Boolean | Has the list the same width than the input | true |
| hasStyle | Boolean | Does the list already has his own style or should freedomplete apply his style | false |
| valsParentID | String | the values's parent ID os if the values are li, the valsParentID will be the ul/ol ID | empty |
| handleShowing | Boolean | If the list need to show and hide or to be always shown | true |
| parentRelative | Boolean | If the input is in a position relative parent | false |
| parentRelativeID | String | if parentRelative is true just pass the parent's ID | empty |
| values | Array | if the values does'nt exist yet, pass them in an array like `[value1, value2, ...]` | empty |
| mode | String | the event that start the values filtering. Can be sendingInput or keypress | keypress |
| timeoutKeypress | Integer | the activation time in milisecond the event will take to trigger. THe mode need to be keypress  | 300 |
| buttonID | String | The button's ID if the mode is sendingInput. | empty |
| clickToApply | Boolean | If the click is needed to apply the value to the input | true |
| multipleVal | Boolean | If we can have multiple values | false |
| ajaxMode | Boolean | If you want to use this with ajax to optimize the number of values | false |
| ajaxParams | Object | the params you need to pass to use ajax the ajax mode correctly | empty |
| applyCallback | function | a callback can be called after the value was applied to the input | null |

The ajaxParams need to have those params to work properly

| Index | Type | Description | Default value |
| --- | --- | --- | --- |
| url | String | The url called | empty |
| method | String | The method of the call | POST |
| nounceMode | String | To protect ajax call a common way is to use a token. Can be headers or value | headers |
| nounceName | String | the name of the nounce | empty |
| token | String | The nounce value | empty |
| valueName | String | the index of the value or the header | empty |
| additionalVal | Object | Used if you want to add additional values to the ajax call `{paramIndex: paramValue}` | empty |
| customizeInputName | Object | Used to customize the name of the value in tje response | empty |

## Author

* **Kevin Goyvaerts** - *Initial work* - [BitBucket](https://bitbucket.org/KevinGoyvaerts)

## License

Freedomplete is licensed under the MIT License
